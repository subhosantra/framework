<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AbbreviationsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AbbreviationsTable Test Case
 */
class AbbreviationsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\AbbreviationsTable
     */
    protected $Abbreviations;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Abbreviations',
        'app.Publications',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Abbreviations') ? [] : ['className' => AbbreviationsTable::class];
        $this->Abbreviations = TableRegistry::getTableLocator()->get('Abbreviations', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Abbreviations);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
