<?php
namespace App\Controller;
// Once the database is updated, bake a new cake. 
// 		1. Test Regex
//		2. Add validation for search and advancedsearch
//		3. Test search adn advancedsearch
use Cake\ORM\TableRegistry;
use App\Controller\AppController;


class SearchController extends AppController
{	
	static function getRegexsForTranslationPhrases($translationPhrases){
        // process phrases
		if (trim(trim($translationPhrases), ",") == "") {
			$translationPhrases = array();
		} else {
			$translationPhrases = explode(
				",", trim($translationPhrases, ","));
		}

        // get the corresponding regexes for each search phrase
		$regexs = array();

        // translation lines start with #tr.xx:
		$translationPrefixPattern = "[[.newline.]][[.#.]]tr\\.[[:alpha:]]{2}[[.colon.]] +";
		$translationPattern = "$translationPrefixPattern.*%s";
		foreach ($translationPhrases as $phrase) {
            // if the given search term is a regular expression,
            // use the given regex directly.
			if (preg_match('/^\/.*\/$/', $phrase)) {
				$regex = substr($phrase, 1, -1);
                // if regex checks starting pattern,
                // check the pattern right after prefix
				if ($regex[0] === '^') {
					$regex = $translationPrefixPattern . substr($regex, 1);
                // else check the pattern anywhere after the prefix
				} else {
					$regex = sprintf($translationPattern, $regex);
				}
			} else {
				$safe_phrase = preg_quote($phrase, '/');
				$regex = sprintf($translationPattern, $safe_phrase);
			}
			$regexs[] = "($regex)";
		}
		return $regexs;
	}

	static function getRegexsForCommentPhrases($commentPhrases){
        // process phrases
		if (trim(trim($commentPhrases), ",") == "") {
			$commentPhrases = array();
		} else {
			$commentPhrases = explode(
				",", trim($commentPhrases, ","));
		}

        // get the corresponding regexes for each search phrase
		$regexs = array();

        // comment lines start with # or $ and a space
		$commentPrefixPattern = "[[.newline.]]([[.#.]]|[[.$.]]) +";
        //regex updated to handle either $ or #. Previous one was resulting in searching random characters.
       // $commentPrefixPattern = "[.newline.][#$] +";
		$commentPattern = "$commentPrefixPattern.*%s";
		foreach ($commentPhrases as $phrase) {
            // if the given search term is a regular expression,
            // use the given regex directly.
			if (preg_match('/^\/.*\/$/', $phrase)) {
				$regex = substr($phrase, 1, -1);
                // if regex checks starting pattern,
                // check the pattern right after prefix
				if ($regex[0] === '^') {
					$regex = $commentPrefixPattern . substr($regex, 1);
                // else check the pattern anywhere after the prefix
				} else {
					$regex = sprintf($commentPattern, $regex);
				}
			} else {
				$safe_phrase = preg_quote($phrase, '/');
				$regex = sprintf($commentPattern, $safe_phrase);
			}
			$regexs[] = "($regex)";
		}
		return $regexs;
	}
	static function getRegexsForStructurePhrases($structurePhrases){
        // process phrases
		if (trim(trim($structurePhrases), ",") == "") {
			$structurePhrases = array();
		} else {
			$structurePhrases = explode(
				",", trim($structurePhrases, ","));
		}

        // get the corresponding regexes for each search phrase
		$regexs = array();

        // comment lines start with # or $ and a space
		$structurePrefixPattern = "[[.newline.]]([[.#.]]|[[.$.]]) +";
        //regex updated to handle either $ or #. Previous one was resulting in searching random characters.
       // $commentPrefixPattern = "[.newline.][#$] +";
		$structurePattern = "$structurePrefixPattern.*%s";

		if(is_array($structurePhrases))
		{


			foreach ($structurePhrases as $phrase) {
            // if the given search term is a regular expression,
            // use the given regex directly.
				if (preg_match('/^\/.*\/$/', $phrase)) {
					$regex = substr($phrase, 1, -1);
                // if regex checks starting pattern,
                // check the pattern right after prefix
					if ($regex[0] === '^') {
						$regex = $structurePrefixPattern . substr($regex, 1);
                // else check the pattern anywhere after the prefix
					} else {
						$regex = sprintf($structurePattern, $regex);
					}
				} else {
					$safe_phrase = preg_quote($phrase, '/');
					$regex = sprintf($structurePattern, $safe_phrase);
				}
				$regexs[] = "($regex)";
			}

		}
		else{
			$phrase=$structurePhrases;
     // if the given search term is a regular expression,
        // use the given regex directly.
			if (preg_match('/^\/.*\/$/', $phrase)) {
				$regex = substr($phrase, 1, -1);
            // if regex checks starting pattern,
            // check the pattern right after prefix
				if ($regex[0] === '^') {
					$regex = $structurePrefixPattern . substr($regex, 1);
            // else check the pattern anywhere after the prefix
				} else {
					$regex = sprintf($structurePattern, $regex);
				}
			} else {
				$safe_phrase = preg_quote($phrase, '/');
				$regex = sprintf($structurePattern, $safe_phrase);
			}
			$regexs[] = "($regex)";
		}

		return $regexs;
	}


	// public function advancedsearch($dic){
	// 	$artifact = TableRegistry::get('Artifacts');
	// 	$query =   $artifact->find();
	// 	$query->matching('Collections');
	// 	$query->matching('Periods');
	// 	$query->matching('Publications.Authors');
	// 	$query->matching('Languages');
	// 	$query->matching('Genres');
	// 	$query->matching('Collections');
	// 	$query->matching('Materials');
	// 	$query->matching('ArtifactTypes');
	// 	$query->matching('Inscriptions');
	// 	$query->matching('Proveniences');
	// 	$query->matching('ArtifactsCredits.Credits.Authors');
	// 	//Validation for None 

	// 	foreach ($dic as $key => $value) {
	// 		switch ($key) {
	// 			case 'Publications':
	// 			$v = '%' . $value .'%' ;
	// 			$query->where(['Publications.title LIKE'=>$v]);
	// 			break;
	// 			/*
	// 			case 'Authors':
	// 			$query->where(['Authors.author'=>$value]);

	// 			break;*/

	// 			case 'Publication_date':
	// 			$query->where(['Publications.publication_date'=>$value]);
				
	// 			case 'CDLI no':
	// 			$query->where(['Artifacts.id'=>$value]);

	// 			break;

	// 			case 'Object':
	// 			$obj_dic = array();
	// 			foreach ($value as $obj) {
	// 				$obj_dic[] = ['ArtifactsTypes.artifact_type'=>$obj];
	// 			}
	// 			$query->where(['OR'=>$obj_dic]);
	// 			break;

	// 			case 'Materials':
	// 			$mat_dic = array();
	// 			foreach ($value as $mat) {
	// 				$mat_dic[] = ['Materials.material'=>$mat];
	// 			}
	// 			$query->where(['OR'=>$mat_dic]);
	// 			break;

	// 			case 'Collections':
	// 			$mat_dic = array();
	// 			foreach ($value as $mat) {
	// 				$mat_dic[] = ['Collections.collection'=>$mat];
	// 			}
	// 			$query->where(['OR'=>$mat_dic]);

	// 			break;


	// 			case 'Proveniences':
	// 			$mat_dic = array();
	// 			foreach ($value as $mat) {
	// 				$mat_dic[] = ['Proveniences.provenience'=>$mat];
	// 			}
	// 			$query->where(['OR'=>$mat_dic]);


	// 			break;
	// 			case 'Periods':
	// 			$mat_dic = array();
	// 			foreach ($value as $mat) {
	// 				$mat_dic[] = ['Periods.period'=>$mat];
	// 			}
	// 			$query->where(['OR'=>$mat_dic]);
	// 			break;

	// 			case 'Transliteraton':
	// 			$v = '%' . $value .'%' ;
	// 			$query->where(['Inscriptions.transliteration LIKE'=>$v]);
	// 			break;

	// 			case 'Translation':
	// 			$query->where(['Inscriptions.transliteraton'=>$this->getRegexsForTranslationPhrases($value)]);

	// 			break;

	// 			case 'Languages':
	// 			$mat_dic = array();
	// 			foreach ($value as $mat) {
	// 				$mat_dic[] = ['Languages.language'=>$mat];
	// 			}
	// 			$query->where(['OR'=>$mat_dic]);
	// 			break;
				
	// 			case 'Genres':
	// 			$mat_dic = array();
	// 			foreach ($value as $mat) {
	// 				$mat_dic[] = ['Genres.genre'=>$mat];
	// 			}
	// 			$query->where(['OR'=>$mat_dic]);
	// 			break;

	// 			case 'ATF Source':
	// 			$query->where(['ArtifactsCredits.credit_type'=>'ATF Source'])->where(['ArtifactsCredits.Credits.Authors'=>$value]);
	// 			break;

	// 			case 'Catalogue Source':
	// 			$query->where(['ArtifactsCredits.credit_type'=>'Catalogue Source'])->where(['ArtifactsCredits.Credits.Authors'=>$value]);
	// 			break;

	// 			case 'Transliteraton Source':
	// 			$query->where(['ArtifactsCredits.credit_type'=>'Transliteraton Source'])->where(['ArtifactsCredits.Credits.Authors'=>$value]);
	// 			break;

	// 			// work on
	// 			case 'Comment':
	// 			$query->where(['Inscriptions.transliteraton'=>$this->getRegexsForCommentPhrases($value)]);
	// 			break;

	// 			case 'Structure':
	// 			$query->where(['Inscriptions.transliteraton'=>$this->getRegexsForStructurePhrases($value)]);
	// 			break;
				

	// 			case 'Object_Remarks':
	// 			$query->where(['Inscriptions.transliteraton'=>$value]);
	// 			break;
	// 		}
	// 	}

	// }


	public function showmore($value = null){
		$artifact = TableRegistry::get('Artifacts');
		$query =   $artifact->find();
		$query->matching('Collections');
		$query->matching('Periods');
		$query->matching('Publications.Authors');
		$query->matching('Languages');
		$query->matching('Genres');
		$query->matching('Collections');
		$query->matching('Materials');
		$query->matching('ArtifactTypes');
		$query->matching('Proveniences');

		$query->where(['Artifacts.id'=>$value]);
		$row = $query->first();
		echo $row;
		
		// debug($query);

		// $results = $query->all();
		// $data = $results->toList();
		// echo $data[0];	
	}


	static function search($Publications ,$Collections, $Proveniences,$Periods,$Transliteraton,$CDLI_no){
		$artifact = TableRegistry::get('Artifacts');
		$query =   $artifact->find();

		if (!empty($CDLI_no)) {
			$query->where(['Artifacts.id IN'=>$CDLI_no]);
		}

		$query->matching('Publications.Authors');

		if (!empty($Publications)) {
			$mat_dic = array();
			foreach ($Publications as $Publication) {
				$mat_dic[] = ['Publications.title LIKE'=>'%' . $Publication .'%'];
			}
			$query->where(['OR'=>$mat_dic]);
		}

		$query->matching('Collections');


		if (!empty($Collections)) {
			$mat_dic = array();
			foreach ($Collections as $Collection) {
				$mat_dic[] = ['Collections.collection LIKE'=>'%' . $Collection .'%'];
			}
			$query->where(['OR'=>$mat_dic]);

		}
		$query->matching('Proveniences');


		if (!empty($Proveniences)) {
			$mat_dic = array();
			foreach ($Proveniences as $Provenience) {
				$mat_dic[] = ['Proveniences.provenience LIKE'=>'%' . $Provenience .'%'];
			}
			$query->where(['OR'=>$mat_dic]);

		}
		$query->matching('Periods');

		if (!empty($Periods)) {
			$mat_dic = array();
			foreach ($Periods as $Period) {
				$mat_dic[] = ['Periods.period LIKE'=>'%' . $Period .'%'];
			}
			$query->where(['OR'=>$mat_dic]);

		}

		$query->matching('Inscriptions');

		if (!empty($Transliteraton)) {
			$mat_dic = array();
			foreach ($Transliteraton as $Tran) {
				$mat_dic[] = ['Inscriptions.transliteration LIKE'=>'%' . $Tran .'%'];
			}
			$query->where(['OR'=>$mat_dic]);
		}


		$query->matching('Materials');
		$query->matching('ArtifactTypes');
		//debug($query);
		$result = $query->all();
		return $result;
	}

    public function initialize()
    {
        parent::initialize();

        $this->Auth->allow(['index']);
    }

	public function index()
	{	
		ini_set('memory_limit', -1);
		$query_array_key = array_keys($this->request->query);
		$Publications = array_intersect_key( $this->request->query, array_flip(preg_grep('/^Publications.*/',$query_array_key)));
		$Collections = array_intersect_key( $this->request->query, array_flip(preg_grep('/^Collections.*/',$query_array_key)));
		$Proveniences = array_intersect_key( $this->request->query, array_flip(preg_grep('/^Proveniences.*/',$query_array_key)));
		$Periods = array_intersect_key( $this->request->query, array_flip(preg_grep('/^Periods.*/',$query_array_key)));
		$Transliteraton = array_intersect_key( $this->request->query, array_flip(preg_grep('/^Transliteraton (\w+)/i',$query_array_key)));
		$CDLI_no = array_intersect_key( $this->request->query, array_flip(preg_grep('/^CDLI.*/',$query_array_key)));
		if (!empty($Publications) or !empty($Collections) or!empty($Proveniences) or!empty($Periods) or!empty($Transliteraton) or!empty($CDLI_no)) {
			$result = $this->search($Publications ,$Collections, $Proveniences,$Periods,$Transliteraton,$CDLI_no);
			$this->set('result',$result);		
		}

	}
	
	public function advancedsearchindex(){
		$Publications = $this->request->query('Publications');
		$Collections = $this->request->query('Collections');
		$Proveniences = $this->request->query('Proveniences');
		$Periods = $this->request->query('Periods');
		$Transliteraton = $this->request->query('Transliteraton');
		$CDLI_no = $this->request->query('CDLI');
		if (!empty($Publications) or !empty($Collections) or!empty($Proveniences) or!empty($Periods) or!empty($Transliteraton) or!empty($CDLI_no)) {
			$result = $this->search($Publications ,$Collections, $Proveniences,$Periods,$Transliteraton,$CDLI_no);
			$this->set('result',$result);		
		}


		$dic = array('Publications' => '', 'Authors' => '', 'Publication_date' => '', 'CDLI no'=>'','Object'=>[],'Materials'=>[] ,'Collections'=> [], 'Proveniences' =>[], 'Periods' => [] , 'Object_Remarks'=>'' , 'Transliteraton' =>'', 'Translation' =>'', 'Languages' =>[],'Genres'=>[], 'CDLI no' =>'','Comment'=>'','Structure'=>'','ATF Source'=>'','Catalogue Source'=>'','Transliteraton Source'=>'');
	}

}


?>