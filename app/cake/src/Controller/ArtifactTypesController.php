<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;


/**
 * ArtifactTypes Controller
 *
 * @property \App\Model\Table\ArtifactTypesTable $ArtifactTypes
 *
 * @method \App\Model\Entity\ArtifactType[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ArtifactTypesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['ParentArtifactTypes']
        ];
        $artifactTypes = $this->paginate($this->ArtifactTypes);

        $this->set(compact('artifactTypes'));
    }

    /**
     * View method
     *
     * @param string|null $id Artifact Type id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $artifactType = $this->ArtifactTypes->get($id, [
            'contain' => ['ParentArtifactTypes', 'ChildArtifactTypes']
        ]);

        $artifacts = TableRegistry::get('Artifacts');
        $count = $artifacts->find('list', ['conditions' => ['artifact_type_id' => $id]])->count();

        $this->set(compact('artifactType','count'));

    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $artifactType = $this->ArtifactTypes->newEntity();
        if ($this->request->is('post')) {
            $artifactType = $this->ArtifactTypes->patchEntity($artifactType, $this->request->getData());
            if ($this->ArtifactTypes->save($artifactType)) {
                $this->Flash->success(__('The artifact type has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The artifact type could not be saved. Please, try again.'));
        }
        $parentArtifactTypes = $this->ArtifactTypes->ParentArtifactTypes->find('list', ['limit' => 200]);
        $this->set(compact('artifactType', 'parentArtifactTypes'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Artifact Type id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $artifactType = $this->ArtifactTypes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $artifactType = $this->ArtifactTypes->patchEntity($artifactType, $this->request->getData());
            if ($this->ArtifactTypes->save($artifactType)) {
                $this->Flash->success(__('The artifact type has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The artifact type could not be saved. Please, try again.'));
        }
        $parentArtifactTypes = $this->ArtifactTypes->ParentArtifactTypes->find('list', ['limit' => 200]);
        $this->set(compact('artifactType', 'parentArtifactTypes'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Artifact Type id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $artifactType = $this->ArtifactTypes->get($id);
        if ($this->ArtifactTypes->delete($artifactType)) {
            $this->Flash->success(__('The artifact type has been deleted.'));
        } else {
            $this->Flash->error(__('The artifact type could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
