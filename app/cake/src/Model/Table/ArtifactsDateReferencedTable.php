<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ArtifactsDateReferenced Model
 *
 * @property \App\Model\Table\ArtifactsTable|\Cake\ORM\Association\BelongsTo $Artifacts
 * @property \App\Model\Table\RulersTable|\Cake\ORM\Association\BelongsTo $Rulers
 * @property \App\Model\Table\MonthsTable|\Cake\ORM\Association\BelongsTo $Months
 * @property \App\Model\Table\YearsTable|\Cake\ORM\Association\BelongsTo $Years
 *
 * @method \App\Model\Entity\ArtifactsDateReferenced get($primaryKey, $options = [])
 * @method \App\Model\Entity\ArtifactsDateReferenced newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ArtifactsDateReferenced[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ArtifactsDateReferenced|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ArtifactsDateReferenced|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ArtifactsDateReferenced patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ArtifactsDateReferenced[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ArtifactsDateReferenced findOrCreate($search, callable $callback = null, $options = [])
 */
class ArtifactsDateReferencedTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('artifacts_date_referenced');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Artifacts', [
            'foreignKey' => 'artifact_id'
        ]);
        $this->belongsTo('Rulers', [
            'foreignKey' => 'ruler_id'
        ]);
        $this->belongsTo('Months', [
            'foreignKey' => 'month_id'
        ]);
        $this->belongsTo('Years', [
            'foreignKey' => 'year_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmpty('id', 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('month_no')
            ->maxLength('month_no', 50)
            ->allowEmpty('month_no');

        $validator
            ->scalar('day_no')
            ->maxLength('day_no', 50)
            ->allowEmpty('day_no');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['id']));
        $rules->add($rules->existsIn(['artifact_id'], 'Artifacts'));
        $rules->add($rules->existsIn(['ruler_id'], 'Rulers'));
        $rules->add($rules->existsIn(['month_id'], 'Months'));
        $rules->add($rules->existsIn(['year_id'], 'Years'));

        return $rules;
    }
}
