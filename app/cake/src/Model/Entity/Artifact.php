<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Artifact Entity
 *
 * @property int $id
 * @property string|null $ark_no
 * @property int|null $credit_id
 * @property string|null $primary_publication_comments
 * @property string|null $cdli_collation
 * @property string|null $cdli_comments
 * @property string|null $composite_no
 * @property string|null $condition_description
 * @property string|null $created
 * @property string|null $date_comments
 * @property string|null $modified
 * @property \Cake\I18n\FrozenDate|null $dates_referenced
 * @property string|null $designation
 * @property string|null $electronic_publication
 * @property string|null $elevation
 * @property string|null $excavation_no
 * @property string|null $findspot_comments
 * @property string|null $findspot_square
 * @property int|null $height
 * @property string|null $join_information
 * @property string|null $museum_no
 * @property string|null $artifact_preservation
 * @property bool|null $is_public
 * @property bool|null $is_atf_public
 * @property bool|null $are_images_public
 * @property string|null $seal_no
 * @property string|null $seal_information
 * @property string|null $stratigraphic_level
 * @property string|null $surface_preservation
 * @property string|null $general_comments
 * @property int|null $thickness
 * @property int|null $width
 * @property int|null $provenience_id
 * @property int|null $period_id
 * @property bool|null $is_provenience_uncertain
 * @property bool|null $is_period_uncertain
 * @property int|null $artifact_type_id
 * @property string|null $accession_no
 * @property int|null $accounting_period
 * @property string|null $alternative_years
 * @property string|null $dumb2
 * @property string|null $custom_designation
 * @property string|null $period_comments
 * @property string|null $provenience_comments
 * @property bool|null $is_school_text
 * @property int|null $written_in
 * @property int|null $is_object_type_uncertain
 * @property int|null $archive_id
 * @property string|null $created_by
 * @property string|null $db_source
 * @property int|null $weight
 * @property string|null $translation_source
 * @property string|null $atf_up
 * @property string|null $atf_source
 *
 * @property \App\Model\Entity\Credit[] $credits
 * @property \App\Model\Entity\Provenience $provenience
 * @property \App\Model\Entity\Period $period
 * @property \App\Model\Entity\ArtifactType $artifact_type
 * @property \App\Model\Entity\Archive $archive
 * @property \App\Model\Entity\ArtifactDate[] $artifact_dates
 * @property \App\Model\Entity\ArtifactsComposite[] $artifacts_composites
 * @property \App\Model\Entity\ArtifactsDateReferenced[] $artifacts_date_referenced
 * @property \App\Model\Entity\ArtifactsSeal[] $artifacts_seals
 * @property \App\Model\Entity\ArtifactsShadow[] $artifacts_shadow
 * @property \App\Model\Entity\Inscription[] $inscriptions
 * @property \App\Model\Entity\RetiredArtifact[] $retired_artifacts
 * @property \App\Model\Entity\Collection[] $collections
 * @property \App\Model\Entity\Date[] $dates
 * @property \App\Model\Entity\ExternalResource[] $external_resources
 * @property \App\Model\Entity\Genre[] $genres
 * @property \App\Model\Entity\Language[] $languages
 * @property \App\Model\Entity\Material[] $materials
 * @property \App\Model\Entity\Publication[] $publications
 */
class Artifact extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'ark_no' => true,
        'credit_id' => true,
        'primary_publication_comments' => true,
        'cdli_collation' => true,
        'cdli_comments' => true,
        'composite_no' => true,
        'condition_description' => true,
        'created' => true,
        'date_comments' => true,
        'modified' => true,
        'dates_referenced' => true,
        'designation' => true,
        'electronic_publication' => true,
        'elevation' => true,
        'excavation_no' => true,
        'findspot_comments' => true,
        'findspot_square' => true,
        'height' => true,
        'join_information' => true,
        'museum_no' => true,
        'artifact_preservation' => true,
        'is_public' => true,
        'is_atf_public' => true,
        'are_images_public' => true,
        'seal_no' => true,
        'seal_information' => true,
        'stratigraphic_level' => true,
        'surface_preservation' => true,
        'general_comments' => true,
        'thickness' => true,
        'width' => true,
        'provenience_id' => true,
        'period_id' => true,
        'is_provenience_uncertain' => true,
        'is_period_uncertain' => true,
        'artifact_type_id' => true,
        'accession_no' => true,
        'accounting_period' => true,
        'alternative_years' => true,
        'dumb2' => true,
        'custom_designation' => true,
        'period_comments' => true,
        'provenience_comments' => true,
        'is_school_text' => true,
        'written_in' => true,
        'is_object_type_uncertain' => true,
        'archive_id' => true,
        'created_by' => true,
        'db_source' => true,
        'weight' => true,
        'translation_source' => true,
        'atf_up' => true,
        'atf_source' => true,
        'credits' => true,
        'provenience' => true,
        'period' => true,
        'artifact_type' => true,
        'archive' => true,
        'artifact_dates' => true,
        'artifacts_composites' => true,
        'artifacts_date_referenced' => true,
        'artifacts_seals' => true,
        'artifacts_shadow' => true,
        'inscriptions' => true,
        'retired_artifacts' => true,
        'collections' => true,
        'dates' => true,
        'external_resources' => true,
        'genres' => true,
        'languages' => true,
        'materials' => true,
        'publications' => true
    ];
}
