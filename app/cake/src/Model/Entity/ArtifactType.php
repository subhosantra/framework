<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ArtifactType Entity
 *
 * @property int $id
 * @property string $artifact_type
 * @property int|null $parent_id
 *
 * @property \App\Model\Entity\ArtifactType $parent_artifact_type
 * @property \App\Model\Entity\ArtifactType[] $child_artifact_types
 * @property \App\Model\Entity\Artifact[] $artifacts
 */
class ArtifactType extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'artifact_type' => true,
        'parent_id' => true,
        'parent_artifact_type' => true,
        'child_artifact_types' => true,
        'artifacts' => true
    ];
}
