<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactsPublication $artifactsPublication
 */
?>
<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <div class="capital-heading"><?= __('View Artifacts Publication') ?></div>

        <table class="table-bootstrap">
            <tbody>
                <tr>
                    <th scope="row"><?= __('Artifact') ?></th>
                    <td><?= $artifactsPublication->has('artifact') ? $this->Html->link($artifactsPublication->artifact->id, ['controller' => 'Artifacts', 'action' => 'view', $artifactsPublication->artifact->id]) : '' ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Publication') ?></th>
                    <td><?= $artifactsPublication->has('publication') ? $this->Html->link($artifactsPublication->publication->title, ['controller' => 'Publications', 'action' => 'view', $artifactsPublication->publication->id]) : '' ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Exact Reference') ?></th>
                    <td><?= h($artifactsPublication->exact_reference) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Publication Type') ?></th>
                    <td><?= h($artifactsPublication->publication_type) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Id') ?></th>
                    <td><?= $this->Number->format($artifactsPublication->id) ?></td>
                </tr>
            </tbody>
        </table>

    </div>

    <div class="col-lg boxed">
        <div class="capital-heading"><?= __('Related Actions') ?></div>
        <?= $this->Html->link(__('Edit Artifacts Publication'), ['action' => 'edit', $artifactsPublication->id], ['class' => 'btn-action']) ?>
        <?= $this->Form->postLink(__('Delete Artifacts Publication'), ['action' => 'delete', $artifactsPublication->id], ['confirm' => __('Are you sure you want to delete # {0}?', $artifactsPublication->id), 'class' => 'btn-action']) ?>
        <?= $this->Html->link(__('List Artifacts Publications'), ['action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Artifacts Publication'), ['action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Artifacts'), ['controller' => 'Artifacts', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Artifact'), ['controller' => 'Artifacts', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Publications'), ['controller' => 'Publications', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Publication'), ['controller' => 'Publications', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
    </div>

</div>



