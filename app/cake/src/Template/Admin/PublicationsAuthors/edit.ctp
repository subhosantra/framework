<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\PublicationsAuthor $publicationsAuthor
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $publicationsAuthor->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $publicationsAuthor->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Publications Authors'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Publications'), ['controller' => 'Publications', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Publication'), ['controller' => 'Publications', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Authors'), ['controller' => 'Authors', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Author'), ['controller' => 'Authors', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="publicationsAuthors form large-9 medium-8 columns content">
    <?= $this->Form->create($publicationsAuthor) ?>
    <fieldset>
        <legend><?= __('Edit Publications Author') ?></legend>
        <?php
            echo $this->Form->control('publication_id', ['options' => $publications, 'empty' => true]);
            echo $this->Form->control('author_id', ['options' => $authors, 'empty' => true]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
